FROM  java:8u66-jre

RUN apt update && apt install -y python-pip 

run pip install tutum

# CLEANUP
RUN apt remove --auto-remove -y python-pip
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /var/tmp/*

CMD ["/bin/bash"]
